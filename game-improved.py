from random import randint

userName = input("Hi! What is your name? ")
numGuesses = input(f"Hi {userName}! How many times will you let me guess your birthday?: ")
numGuesses = int(numGuesses) #try to take out ints

# the range is 1924 to 2004
lowYear = 1924
highYear = 2004

lowMonth = 1
highMonth = 12

noGuess = False
# helps the number round down without using int ()(lowYear+highYear)//2
computerGuessYear = int((lowYear+highYear)/2)
computerGuessMonth = int((lowMonth+highMonth)/2)

print(f"Is your birth year {computerGuessYear}?") #get answer for year first


for i in range(numGuesses): #[0, 1, 2]
# ask the user if their birthday is earlier or later or correct
    higherOrLower = input("Answer correct, earlier, or later: ")
    if higherOrLower == "correct":
        birthYear = computerGuessYear
        break
    elif i >= range(numGuesses)[-1]:
        print("I am so sorry! I could not guess your birthday.")
        noGuess = True
        quit()
    elif higherOrLower == "earlier":
        highYear = computerGuessYear
        computerGuessYear = int((lowYear+highYear)/2)
        print(f"Is your birth year {computerGuessYear}?")
        # switch the high with the computerGuess
    elif higherOrLower == "later": # else if the birthday is later
        lowYear = computerGuessYear
        computerGuessYear = int((lowYear+highYear)/2)
        print(f"Is your birth year {computerGuessYear}?")
        # switch the low with the computer's guess
    else:
        print("You have entered incorrect input. Try again.")
    # else you've entered incorrect input

if noGuess:
    quit()


print(f"Is your birth month {computerGuessMonth}?")

for i in range(numGuesses):
    higherOrLower = input("Answer correct, earlier, or later: ")
    if higherOrLower == "correct":
        birthMonth = computerGuessMonth
        print(f"Yay! Your birthday is {birthMonth}/{birthYear}")
        break
    elif i >= range(numGuesses)[-1]:
        print("I am so sorry! I could not guess your birthday.")
        noGuess = True
        quit()
    elif higherOrLower == "earlier":
        highMonth = computerGuessMonth
        computerGuessMonth = int((lowMonth+highMonth)/2)
        print(f"Is your birth month {computerGuessMonth}?")
        # switch the high with the computerGuess
    elif higherOrLower == "later": # else if the birthday is later
        lowMonth = computerGuessMonth
        computerGuessMonth = int((lowMonth+highMonth)/2)
        print(f"Is your birth month {computerGuessMonth}?")
        # switch the low with the computer's guess
    else:
        print("You have entered incorrect input. Try again.")
    # else you've entered incorrect input

computerGuessDay = randint(0,31)
lowDay = 0
highDay = 31
print(f"Is your day of birth {computerGuessDay}?")

for i in range(numGuesses):
    higherOrLower = input("Answer correct, earlier, or later: ")
    if higherOrLower == "correct":
        dayOfBirth = computerGuessDay
        print(f"Yay! Your birthday is {birthMonth}/{dayOfBirth}/{birthYear}")
        break
    elif i >= range(numGuesses)[-1]:
        print("I am so sorry! I could not guess your birthday.")
        noGuess = True
        quit()
    elif higherOrLower == "earlier":
        highDay = computerGuessDay
        computerGuessDay = int((lowDay+highDay)/2)
        print(f"Is your day of birth {computerGuessDay}?")
        # switch the high with the computerGuess
    elif higherOrLower == "later": # else if the birthday is later
        lowDay = computerGuessDay
        computerGuessDay = int((lowDay+highDay)/2)
        print(f"Is your day of birth {computerGuessDay}?")
        # switch the low with the computer's guess
    else:
        print("You have entered incorrect input. Try again.")
    # else you've entered incorrect input
